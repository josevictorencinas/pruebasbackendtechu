require('dotenv').config();

// cargamos el framework
const express = require('express');
const io = require('./io'); //cargamos el modulo para acceder a disco y escribir ficheros
const userController =require('./controllers/UserController');
const authController =require('./controllers/AuthController');
const accountController =require('./controllers/AccountController');
const app = express(); //inicializamos el framework

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(express.json()); //Se especifica que por defecot use json como tipo de body
app.use(enableCORS);

//Asignamos un puerto cogiendo la variable deentorno, si no utilizamos 3000 como valor por defecto
const port = process.env.PORT || 3000;

//iniciamos el servidor en el puerto
app.listen(port);

console.log("API escuchando en el puerto asasas bib bib "+port);

//Registramos una direccion para que se atienda la peticion
app.get("/apitechu/V1/hello",
   function(req,res){
     console.log("GET /apitechu/V1/hello")
//Devolvemos una respuesta
     res.send({"msg":"Hola desde API TechU"});
   }

)

//Registramos una direccion para que se atienda la peticion
app.get("/apitechu/V1/users",userController.getUsersV1);
app.get("/apitechu/V2/users",userController.getUsersV2);
app.get("/apitechu/V2/users/:id",userController.getUserByIdV2);


//Funcionalidad para hacer el alta de un usuarios
app.post("/apitechu/V1/users", userController.createUserV1);
app.post("/apitechu/V2/users", userController.createUserV2);

//Funcion que borra un usuario del ficheros

app.delete ("/apitechu/V1/users/:id",userController.deleteUserV1);
app.delete ("/apitechu/V2/users/:id",userController.deleteUserV2);

//Registramos las url del controlador
app.post ("/apitechu/V1/login",authController.loginV1);
app.post ("/apitechu/V1/logout/:id",authController.logoutV1);

//Registramos las url del controlador
app.post ("/apitechu/V2/login",authController.loginV2);
app.post ("/apitechu/V2/logout/:id",authController.logoutV2);

//Registramos la url de controlador de cuentas
app.get("/apitechu/V1/accounts/:userid",accountController.getAccountsByIdV1);

//Peticion con todos los parametros
app.post("/apitechu/v1/monstruo/:p1/:p2",
function(req,res){
  console.log("Parametros");
  console.log(req.params);

  console.log("Query");
  console.log(req.query);


  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);
}

)
