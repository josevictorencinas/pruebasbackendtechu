
const fs= require('fs');

//Función externa que escribe en el fichero de usuarios
function writeUserDataToFile(data){
  console.log("writeUserDataToFile");


  var jsonUserData = JSON.stringify(data); //convertimos el array a un JSON

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function (err){
      if (err){
      console.log(err);
    }else{
      console.log("usuario persistido en fichero de usuarios");
    }
  }

  )
}


// OJO QUE ESTO A VECES SE OLVIDA
module.exports.writeUserDataToFile = writeUserDataToFile;
