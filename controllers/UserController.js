

//Traemos los modulos necesarios
const io = require('../io'); //cargamos el modulo para acceder a disco y escribir ficheros
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLabURL='https://api.mlab.com/api/1/databases/apitechuvel13ed/collections/';
const MLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;

function getUsersV1(req,res){
  console.log("GET /apitechu/V1/users")
  // cargamos el archivo usuarios.json en una variable
  var users = require('../usuarios.json');

  resultado = {};

  // Recuperamos los valores de los parametros
  console.log ("$count "+req.query.$count);
  console.log ("$top "+req.query.$top);

  // Verificamos si viene el parametro $count y si es así rellenamos el parametro count.
  if (req.query.$count == "true"){
    resultado.count = users.length;
    console.log ("el tamaño del array es:" + resultado.count);
  }

  //Verificamos si viene el parametro $top. Si el $top no viene se mete la lista entera
  // resultado.usuarios = req.query.$top? asignacion 1 : asignacion2.
  if (req.query.$top != undefined){
    console.log ("Viene el parametro $top");
    var total_devolver = req.query.$top;
    console.log ("Se devolveran " +total_devolver);
    var afinal = users.slice(0,total_devolver);
    console.log ("resultado" + afinal);
    resultado.usuarios = afinal;
  }else{
    console.log ("No viene el parametro $top");
    // Añadimos la lista completa
    resultado.usuarios = users;
  }

  res.send(resultado);

  //Enviamos el fichero usuarios.json. El segundo parametro dice el directorio donde debe partir la busqueda del fichero
  //res.sendfile('usuarios.json',{root: __dirname});
}

function getUsersV2(req,res){
  console.log("GET /apitechu/V2/users")
//Creamos el cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  httpClient.get("user?" +MLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body:{
        "msg": "Error obteniendo usuarios"
      }
      res.send(response);
    }

  );

}

function getUserByIdV2(req,res){
console.log("GET /apitechu/V2/users/:id")

//var id= req.params.id;
console.log("La id del usuario a buscar es: "+id);

//Creamos el cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  //Construimos los parametros
//  var query='q={"id":'+id+'}';
var id= Number.parseInt(req.params.id);
console.log("La id del usuario a buscar es: "+id);
var query='q='+JSON.stringify({"id":id});
console.log("query es "+ query);

  console.log("user?" +query+MLabAPIKey);

  httpClient.get("user?" +query+"&"+MLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response ={
          "msg":"error obteniendo usuario"
        }
        res.status(500);
      }else{
          if(body.length >0){
            var response =body[0];
          }else{
            var response ={
              "msg":"Usuario no encontrado"
            }
            res.status(404);
          }

      }

      res.send(response);
    }

  );

}

function createUserV1(req,res){
  console.log("POST /apitechu/V1/users")
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser ={
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  }

  console.log (newUser);

  var users = require ('../usuarios.json');

  users.push (newUser);
  console.log ("Usuario añadido al array");
  //persistimos el usuario en el fichero
  io.writeUserDataToFile(users);
  console.log ("Proceso de añadir finalizado");

   }

   function deleteUserV1(req,res){
     console.log ("DELETE /apitechu/V1/users/:id -- ");
     console.log ("El ID a borrar es: " +req.params.id);

     var users = require ("../usuarios.json");
     var posicion
   //   //1 - Buscamos el elemento con una estructura for
   //  for (var i=0; i<users.length;i++){
   //    console.log ("posicion i= "+i+" parameto de entrada: "+req.params.id +" +users["+i+"].id: "+users[i].id + " posicion a borrar: " + posicion);
   //    if (users[i].id == req.params.id) posicion=i;
   //  }
     // 2 - Buscamos el elemento utilizando el for in
     // for (identificador in users){
     //   //console.log("Identificador: " +identificador);
     //   if(users[identificador].id==req.params.id)
     //   posicion=identificador;
     // }

     //3. for each
     //   var contador=0;
     //   users.forEach(function (user) {
     //     if (user.id==req.params.id)
     //     posicion=contador;
     //     contador ++;
     //   }
     // )

 //4.- For of
 var contador=0
 for (user of users){
   if (user.id==req.params.id) {
     posicion = contador;
   }
   contador++;
   //posicion=users.indexOf(user);
   console.log("La posición a borrar es." +posicion);

 }

 //5 Array findIndex

 // posicion = users.findIndex (function verificar(user){
 //   return user.id==req.params.id;
 // })
 console.log("posicion"+posicion);

     users.splice (posicion,1);
     console.log("usuario quitado del array");
     io.writeUserDataToFile(users);
   }

   function createUserV2(req,res){
     console.log("POST /apitechu/V1/users")
     //Funcion que crea lun usuario en mongodb

     console.log(req.body.first_name);
     console.log(req.body.id);
     console.log(req.body.last_name);
     console.log(req.body.email);
     console.log(req.body.password);

     var newUser ={
       "id":req.body.id,
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "email" : req.body.email,
       "password": crypt.hash(req.body.password)
     }

     console.log (newUser);

     //Creamos el cliente http
       var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente creado");

       httpClient.post("user?"+MLabAPIKey,newUser,
       function(err,resMLab,body){
         console.log("Usuario creado en Mlab");
         res.status(201).send({"msg":"Usuario creado OK"});
       }
     )



   }

   function deleteUserV2(req,res){
     console.log ("DELETE /apitechu/V2/users/:id -- ");

     //Construimos los parametros
   //  var query='q={"id":'+id+'}';
   var id= Number.parseInt(req.params.id);
   console.log("La id del usuario a buscar es: "+id);
   var query='q='+JSON.stringify({"id":id});
   console.log("query es "+ query);


   //Creamos el cliente http
     var httpClient = requestJson.createClient(baseMLabURL);
     console.log("Cliente creado");

     //montamos el body a enviar
     var bodyborrar=[{}];
     //montamos la cabecera para que sea json
console.log(baseMLabURL+"user?"+query+"&"+MLabAPIKey);
     httpClient.put("user?"+query+"&"+MLabAPIKey,bodyborrar,
    function(err,resMLab,body){
      console.log(resMLab);

      if(body.removed)
        res.status(200).send({"msg":"Usuario borrado"});
      else
        res.status(404).send({"msg":"Usuario inexistente"});


    }
  )


   }


module.exports.getUsersV1=getUsersV1;
module.exports.getUsersV2=getUsersV2;
module.exports.getUserByIdV2=getUserByIdV2;

module.exports.createUserV1=createUserV1;
module.exports.createUserV2=createUserV2;
module.exports.deleteUserV1=deleteUserV1;
module.exports.deleteUserV2=deleteUserV2;
