//Traemos los modulos necesarios
const io = require('../io'); //cargamos el modulo para acceder a disco y escribir ficheros
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLabURL='https://api.mlab.com/api/1/databases/apitechuvel13ed/collections/';
const MLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;

//funcion que recupera las cuentas a traves del codigo de usuarios
function getAccountsByIdV1 (req, res){
  console.log ("GET /apitechu/V1/accounts");

  //Creamos el cliente http
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    //Construimos los parametros
  //  var query='q={"id":'+id+'}';
  var id= Number.parseInt(req.params.userid);
  console.log("La id del usuario a buscar es: "+id);

  //Montamoms la query
  var query='q='+JSON.stringify({"userid":id});
  console.log("query es "+ query);

  console.log("account?" +query+"&"+MLabAPIKey);


    httpClient.get("account?" +query+"&"+MLabAPIKey,
      function(err, resMLab, body){
        if (err){
          var response ={
            "msg":"error obteniendo Cuentas"
          }
          res.status(500);
        }else{
            if(body.length >0){
              var response =body;
            }else{
              var response ={
                "msg":"Cuenta no encontrada"
              }
              res.status(404);
            }

        }

        res.send(response);
      }

)



}

module.exports.getAccountsByIdV1=getAccountsByIdV1;
