
const io = require ('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLabURL='https://api.mlab.com/api/1/databases/apitechuvel13ed/collections/';
const MLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;



function loginV1(req,res){
console.log("POST /apitechu/V1/loginV1");

//recuperamos los parametros
var email = req.body.email;
var password = req.body.password;

console.log ("el email recibido es:" + email);
console.log ("la pass recibida es:"+password);

var users = require("../usuarios.json");
//contador del bucle
var i;
// variable del id de retorno
var idUsuario
for (i=0;i<users.length;i++){
  if ((users[i].email == email)&&(users[i].password == password)){
    idUsuario =users[i].id;
    users[i].logged =true;
    console.log ("Se actualiza el atributo logged:"+users[i].logged);
    io.writeUserDataToFile(users);
    console.log ("El id a devolver será:"+idUsuario);
    break;
  }
}




//Montamos el mensaje de respuesta
var msg = idUsuario ? "Login OK" : "Error en las credenciales suministradas";
console.log (msg);

//Devolvemos el resultado
var resultado={};
resultado.msg=msg;
resultado.idusuario=idUsuario;

res.send(resultado);
}

function logoutV1(req,res){
console.log("POST /apitechu/V1/logoutV1");

//Cargamos los usuarios
var users = require("../usuarios.json");

//recuperamos el id de usuarios
var idUsuario = req.params.id;
console.log ("El usuario a deslogar es:" + idUsuario);
var msg = "Logout incorrecto";
//Inicializamos el resultado a devolver
var resultado={};
var i;
for (i=0;i<users.length;i++){
  if((users[i].id == idUsuario) && (users[i].logged == true) ){
    //borramos el atributo logged
    delete users[i].logged;
    msg ="logout correcto";
    resultado.id = idUsuario;
    //Actualizamos la lista de usuarios
    io.writeUserDataToFile(users);
    break;
  }
}

resultado.msg =msg;
res.send(resultado);

}

function loginV2(req,res){
  console.log("POST /apitechu/V2/loginV2");

  //recuperamos los parametros
  var email = req.body.email;
  var password = req.body.password;

  console.log ("el email recibido es:" + email);
  console.log ("la pass recibida es:"+password);

//MOntar el cliente para consultar el usuario introducido
//Creamos el cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = 'q='+JSON.stringify({"email":email});

  console.log("query es "+ query);

  console.log("user?" +query+'&'+MLabAPIKey);

  httpClient.get("user?" +query+"&"+MLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response ={
          "msg":"error obteniendo usuario"
        }
        res.status(500).send(response);
      }else{
          if(body.length >0){
            var response =body[0];
            //Metemos la comparación de passwords y demas acciones
            console.log (body[0]);
            if(crypt.checkPassword(password,body[0].password)){
              //Actualizar la BBDD con el parametro logged
              var bodylogged = {"$set":{"logged":true}};

              httpClient.put("user?"+query+"&"+MLabAPIKey,bodylogged,
             function(err2,resMLab2,body2){
               console.log(resMLab2);

               if (!err2){
                 var response ={
                   "msg":"Usuario logado correctamente",
                   "id": body[0].id
                 }
                 res.status(200).send(response);
               }else{
                 var response ={
                   "msg":"Internal error"
                 }
                 res.status(500).send(response);

               }

             }
           )



            }else{
              var response ={
                "msg":"Credenciales incorrectas"
              }
              res.status(403).send(response);
            }
          }else{
            var response ={
              "msg":"Usuario no encontrado"
            }
            res.status(404).send(response);
          }

      }

      //res.send(response);
    }
)


}


function logoutV2(req,res){
  console.log("POST /apitechu/V2/logoutV2");

  //recuperamos el id de usuarios
  var idUsuario = Number.parseInt(req.params.id);
  console.log ("El usuario a deslogar es:" + idUsuario);

  //MOntar el cliente para consultar el usuario introducido
  //Creamos el cliente http
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    var query = 'q='+JSON.stringify({"id":idUsuario});

    console.log("query es "+ query);

    console.log("user?" +query+'&'+MLabAPIKey);

    httpClient.get("user?" +query+"&"+MLabAPIKey,
      function(err, resMLab, body){
        if (err){
          var response ={
            "msg":"error obteniendo usuario"
          }
          res.status(500).send(response);
        }else{
            if(body.length >0){
              var response =body[0];
              //verificamos si el usuario esta logado
              if(body[0].logged == true){

                var bodylogged = {"$unset":{"logged":""}};

                httpClient.put("user?"+query+"&"+MLabAPIKey,bodylogged,
               function(err2,resMLab2,body2){
                 console.log(resMLab2);

                 if (!err2){
                   var response ={
                     "msg":"Usuario  deslogado correctamente"
                   }
                   res.status(200).send(response);
                 }else{
                   var response ={
                     "msg":"Internal error"
                   }
                   res.status(500).send(response);

                 }

               }
             )

              }else{
                var response ={
                  "msg":"Usuario no logado"
                }
                res.status(403).send(response);
              }
            }else{
              var response ={
                "msg":"Usuario no encontrado"
              }
              res.status(404).send(response);
            }

        }

        //res.send(response);
      }

    );




}

module.exports.loginV1=loginV1;
module.exports.logoutV1=logoutV1;
module.exports.loginV2=loginV2;
module.exports.logoutV2=logoutV2;
