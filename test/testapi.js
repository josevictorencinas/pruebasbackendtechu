//CArgammos los modulos necesarios para hacer los test unitarios.

const mocha =require ('mocha');
const chai =require ('chai');
const chaihttp =require ('chai-http');

chai.use(chaihttp);

var should = chai.should();

//Describe permite agrupar test unitarios que estén proximos
describe( "First test",
  function(){
    it('Test that Duckduckgo works', function(done){
      chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function (err,res){ //En esta función iran las aserciones
            console.log("Request finished");
            //console.log(res);
            console.log(err);
            res.should.have.status(200);
            done();
          }
        )
    }
   )
  }

)


describe( "Test de API Usuarios",
  function(){
    it('Prueba de que la API de ususuarios responde', function(done){
      chai.request('http://localhost:3000')
        .get('/apitechu/V1/hello')
        .end(
          function (err,res){ //En esta función iran las aserciones
            console.log("Request finished");
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU");
            done();
          }
        )
    }
  ),
  it('Prueba de que la API devuelve una lista de usuarios correctos', function(done){
    chai.request('http://localhost:3000')
      .get('/apitechu/V1/users')
      .end(
        function (err,res){ //En esta función iran las aserciones
          console.log("Request finished");
          res.should.have.status(200);
          res.body.usuarios.should.be.a("array");
          for (user of res.body.usuarios){
            user.should.have.property("email");
            user.should.have.property("first_name");
          }
          done();
        }
      )
  }
 )

  }

)
