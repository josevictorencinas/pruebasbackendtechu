const bcrypt = require('bcrypt');

function hash (data){
  console.log("hashing data");
  return bcrypt.hashSync(data,10); //el segundo parametro son las pasadas que hace para cifrar
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}//checkPassword


module.exports.hash =hash;
module.exports.checkPassword =checkPassword;
