#iMAGEN RAIZ

FROM node

#carpeta raiz
WORKDIR /apitechu

#Copia de archivo de proyecto
ADD . /apitechu

#instalo los paquetes necesarios
RUN npm install --only-prod

#abrir puerto que expone
EXPOSE 3000

#COMANDO DE INICIALIZACION
CMD ["node","server.js"]
